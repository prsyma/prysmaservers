from flask import Flask, session, redirect, url_for, escape, request, Response
import requests
import redis
import json
from apscheduler.schedulers.background import BackgroundScheduler
app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
cache = redis.Redis(host='127.0.0.1', port=6379)
worldIP = "http://127.0.0.1:5000"
currentIp="http://127.0.0.1:5001"
areaid = '01'
#id:userclass
class user:
    def __init__(self, identifier, x, y, z, currentzone, health):
        self.identifier = identifier
        self.x = x
        self.y = y
        self.z = z
        self.currentzone = currentzone
        self.health = health
    def updateLocation(self, x,y,z):
        self.x = x
        self.y = y
        self.z = z
    def updateZone(self, newZone):
        self.currentzone = newZone
    def updateHealth(self, health):
        self.health = health
    def getIdentifier(self):
        return self.identifier
    def getX(self):
        return self.x
    def getY(self):
        return self.y
    def getZ(self):
        return self.z
    def getHealth(self):
        return self.health
    def getCurrentZone(self):
        return self.currentzone
        
    
users = {}
areas  ={}
usersConnection = {}
connectionServers = {}
def sendUpdate():
    #build this
    data = cache.scan(match='01user*')
    datal = {}
    for key in data:
        if key == 0:
            continue
        for val in key:
        #print(val.decode('utf-8'))
            datau = cache.get(val.decode('utf-8'))
        #print(datau)
            datal[val.decode('utf-8')] = datau.decode('utf-8')
    
    cache.set('update01', json.dumps(datal))
    return
scheduler = BackgroundScheduler()
scheduler.add_job(func=sendUpdate, trigger="interval", seconds=0.2)
scheduler.start()
@app.before_first_request
def startup():
    #send the server ip as well as area code to the world server
    requests.post(worldIP+'/registerArea', data={'id':areaid, 'ip':currentIp})
    cache.set('01','http://127.0.0.1:5001')
@app.route('/', methods=['GET'])
def main():
    return "Please auth"

@app.route('/update', methods=['POST'])
def update():
    if request.method == 'POST':
        #update user
        uid = request.form['id']
        #do update properly
        data = cache.get('01user'+str(uid))
        dataSet = json.loads(data)
        dataSet['x'] = str(request.form['x'])
        dataSet['y'] = str(request.form['y'])
        dataSet['z'] = str(request.form['z'])
        dataSet['health'] = str(request.form['health'])
        dataSet['area'] = str(request.form['area'])
        cache.set('01user'+str(uid), json.dumps(dataSet))
        #will need futher checking later on
        return Response("{'Meow':'Not Meow'}", status=200, mimetype='application/json')
    return Response("{'Error':'Not Authd'}", status=403, mimetype='application/json')
@app.route('/transfer', methods=['POST'])
def transfer():
    newServer = request.form['newServer']
    uid = request.form['uid']
    data = cache.get('01user'+str(uid))
    cache.set(newServer+'user'+str(uid), data)
    cache.delete('01user'+str(uid))
   
    return

@app.route('/logout', methods=['POST'])
def logout():
    userId = request.form['user']
    #send current user info to the world server to save
    data = cache.get('01user'+str(uid))
    dat = json.loads(data)
    currentCo = dat['x']+','+dat['y']+','+dat['z']
    requests.post(worldIP+'/save', data={'id':userId, 'areaID':users[userId].currentzone, 'coordinate':currentCo, 'health':users[userId].health})

