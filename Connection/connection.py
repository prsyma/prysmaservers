from flask import Flask, session, redirect, url_for, escape, request, Response
import requests
import json
import redis
from flask import jsonify
from apscheduler.schedulers.background import BackgroundScheduler

worldIP = "http://127.0.0.1:5000/"
cache = redis.Redis(host='127.0.0.1', port=6379)
#we need to enumerate the actual areas
areaServers ={"00","01","02","03"}
#username as key or user id and actual id number as thing as well as ip
#id:{}
users={}
app = Flask(__name__)

cache = redis.Redis(host='127.0.0.1', port=6379)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

def valid_login(user, password):
    data = requests.post(worldIP+'authUser', data={'user':user,'pass':password}).content
    #json string
    #'ID':'asdas'
    #if id is greater than 0 then we are okay
    #we can then set up the users
    vals = data.decode('utf8').replace("'", '"')
    y = json.loads(vals)
    if(y['ID'] > 0):
        session['server'] = y['SERVER']
        session['serverip'] = cache.get(y['SERVER'])
    return y['ID']

@app.route('/', methods=['GET'])
def main():
    return "Please auth"


    
@app.route('/authenticate', methods=['POST', 'GET'])
def Authenticate():
    error = None
    if request.method == 'POST':
        userId = valid_login(request.form['username'], request.form['password'])
        if userId > 0:
            session['username'] = request.form['username']
            session['uid'] = userId
            return Response("{{'login':'Success'}, {'ID':"+str(userId)+"}}", status=200, mimetype='application/json')
        else:
            return Response("{'Autehnticate':'Not found'}", status=401, mimetype='application/json')
    else:
        return Response("{'Autehnticate':'Failed'}", status=400, mimetype='application/json')
@app.route('/logout', methods=['POST', 'GET'])
def Logout():
    if request.method == 'POST':
        if 'username' in session:
            requests.post(areaServers[users[session['uid']]]+'/logout', data={'user':session['uid']})
            del users[session['uid']]
            session.pop('username', None)
            session.pop('uid', None)
            session.pop('server', None)
            return Response(jsonify({'logout': 'success'}), status=200, mimetype='application/json')
    return Response(jsonify({'logout': 'failed'}), status=403, mimetype='application/json')
@app.route('/send', methods=['POST','GET'])
def Forward():
    if request.method == 'POST':
        if 'username' in session:
            #check if it is location update if it is transfer user if required
            ip = session['serverip'].decode()+"/update"
            return requests.post(ip, data=request.form).content
    return Response("{'Error':'Not Authd'}", status=403, mimetype='application/json')
@app.route("/get_my_ip", methods=["GET"])
def get_my_ip():
    data = requests.post(worldIP+'getAreas').content
    vals = data.decode('utf8').replace("'", '"')
    y = json.loads(vals)
    areaServers = y
    return jsonify(areaServers), 200
@app.route("/get", methods=["GET"])
def get():
    return jsonify(areaServers), 200
@app.route("/GetUpdates", methods=["GET"])
def getUpdates():
    #we get stuff
    data = cache.scan(match='update*')
    finalData = {}
    for key in data:
        if key == 0:
            continue
        for val in key:
            datat = cache.get(val.decode('utf-8'))
            finalData[val.decode('utf-8')] = datat.decode('utf-8')
    return jsonify(finalData), 200
