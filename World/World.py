from flask import Flask, session, redirect, url_for, escape, request, Response, jsonify
import requests
import redis
import json
from flask_sqlalchemy import SQLAlchemy
app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://world1:world1@127.0.0.1:3306/world'
areas = {}
db = SQLAlchemy(app)
cache = redis.Redis(host='127.0.0.1', port=6379)
class Users(db.Model):
    ID = db.Column(db.Integer, primary_key=True)
    USERNAME = db.Column(db.String(50))
    PASSWORD = db.Column(db.String(100))
    AREAID = db.Column(db.String(2))
    COORDINATE = db.Column(db.String(50))
    HEALTH = db.Column(db.Integer)
    def __repr__(self):
        return '<user %r>'% self.USERNAME


@app.route('/', methods=['GET'])
def main():
    
    query = Users.query.all()
    st = ""
    for c in query:
        st = st+ str(c.ID)+c.USERNAME+c.PASSWORD
    return jsonify(st), 200


@app.route('/getAreas', methods=['POST','GET'])
def getAreas():
    return jsonify(areas),200

@app.route('/save', methods=['POST'])
def save():
    if request.method == 'POST':
        #update user
        userID = str(request.form['id'])
        user = Users.query.filter_by(ID=userID).first()
        if(user != None):
            user.AREAID = request.form['areaID']
            user.COORDINATE = request.form['coordinate']
            user.HEALTH = request.form['health']
            db.session.commit()
        #will need futher checking later on
        return jsonify({'User':'Loggedout'}), 200
    return Response("{'Error':'Not Authd'}", status=403, mimetype='application/json')

@app.route('/registerArea', methods=['POST', 'GET'])
def register_area():
    areaID = request.form['id']
    ip = request.form['ip']
    areas[areaID] = ip
    return Response(200)
@app.route('/authUser', methods=['POST', 'GET'])
def verifyUser():
    #access data base with the names
    user = request.form['user']
    password = request.form['pass']
    #compare to database
    userid = -1
    area=-1
    query = Users.query.filter_by(USERNAME=user, PASSWORD=password).first()
    if(query != None):
        userid = query.ID
        area = query.AREAID
        uuserdata = {}
        uuserdata['id'] = userid
        uuserdata['area']  =area
        temp = query.COORDINATE.split(",")
        uuserdata['x'] = temp[0]
        uuserdata['y'] = temp[1]
        uuserdata['z'] = temp[2]
        uuserdata['health'] = query.HEALTH
        cache.set(area+'user'+str(userid), json.dumps(uuserdata))
    return jsonify({'ID':userid, 'SERVER':area}), 200
